#ifndef _UI_HEADER_
#define _UI_HEADER_


void ui_init(void);
void ui_destroy(void);
void ui_update(void);
void ui_render(void);

#endif // _UI_HEADER_
