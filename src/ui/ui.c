#include "ui.h"
#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include "../player.h"
#include "../window/window.h"

//extern controlled_player player[2];

static ALLEGRO_FONT* ui_font;
static ALLEGRO_BITMAP *shot_bar_spr;




typedef struct ui_bar_c {
    float x;
    float y;
    float w;
    float h;
}ui_bar;


ui_bar shot_bar_p1;
ui_bar shot_bar_p2;
char buf[56];



void ui_init(void){
    Player *p1;
    WeaponConfig *w_cfg;

    p1 = &players[0];
    w_cfg = &weapon_config[p1->weapon];

    ui_font = al_create_builtin_font();

    shot_bar_spr = al_create_bitmap(250,25);

    shot_bar_p1.w = w_cfg->counter + ((w_cfg->timer - w_cfg->counter) / w_cfg->timer) * 100;
    shot_bar_p1.h = 10;



    al_set_target_bitmap(shot_bar_spr);
    al_clear_to_color(al_map_rgba(255,0,0,127));
    RESET_DISPLAY();



}


static void update_bar(float x, float y){

     Player *p1;
     WeaponConfig *w_cfg;

    p1 = &players[0];
    w_cfg = &weapon_config[p1->weapon];

    float pct =  w_cfg->counter + ((w_cfg->timer - w_cfg->counter) / w_cfg->timer) * 100;

    shot_bar_p1.w = pct;
    shot_bar_p1.h = 10;
    shot_bar_p1.x = x;
    shot_bar_p1.y = y;

    sprintf(buf, "P1 Reload: %.2f",pct);
    sprintf(buf, "X: %.2f Y: %.2f", mInput.x, mInput.y);
}

void ui_update(void){

    update_bar(50, window_get_height() - 50);



	
}



void ui_render(void){


     al_draw_scaled_bitmap(shot_bar_spr, 0,0, shot_bar_p1.w, shot_bar_p1.h, shot_bar_p1.x, shot_bar_p1.y , shot_bar_p1.w, 25,0);
     al_draw_text(ui_font, al_map_rgb(255,255,255), 50, window_get_height() - 50, 0, buf);
}

void ui_destroy(void){

     al_destroy_font(ui_font);
}
