#ifndef _MOUSE_HEADER_
#define _MOUSE_HEADER_

typedef struct MouseInput {
    float x;
    float y;
    int rButton;
    int lButton;
    float mWheelUp;
    float mWheelDown;
}MouseInput;

#endif
