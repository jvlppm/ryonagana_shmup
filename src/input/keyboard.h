#ifndef _KEYBOARD_HEADER_
#define _KEYBOARD_HEADER_

#include <allegro5/allegro5.h>

uint32_t keys[ALLEGRO_KEY_MAX];

void keyboard_init();

#endif
