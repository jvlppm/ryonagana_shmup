#include "player.h"

#include "graphic/animation.h"
#include "sound_res.h"
#include "window/window.h"
#include "input/keyboard.h"

#include <stdio.h>
#include <math.h>

#include "utils/math_helper.h"

ALLEGRO_BITMAP *ship_sprite;
animation_sprite projectile_sprite;
Shot shotList[MAX_SHOTS];

int old_dir;
extern uint32_t keys[ALLEGRO_KEY_MAX];

WeaponConfig weapon_config[MAX_SHOTS] = {
    {
        PLAYER_WCANNON,
        0,
        0,
        30,
        1.0f,
        SFX_PLAYER_LASER1
    },
    {
        PLAYER_WLASER,
        0,
        0,
        40,
        1.0f,
        SFX_PLAYER_BASIC
     }
};

int blink_on = FALSE;
int blink_count = 0;
int blink_timer = 50;


void player_init(){
    int i;

    keyboard_init();

    ship_sprite = al_load_bitmap("assets/ship.bmp");
    al_convert_mask_to_alpha(ship_sprite, al_map_rgb(255,0,255));
    shot_start(shotList, &projectile_sprite, "shot01", "assets//shot01.bmp");

    for(i = 0; i < 2; i++){
        players[i].bomb = 0;
        players[i].flags.all_flags = 0U;
        players[i].life = 100.0f;
        players[i].weapon = PLAYER_WCANNON;
        players[i].flags.flag.alive |= 1;
        players[i].w = al_get_bitmap_width(ship_sprite);
        players[i].h = al_get_bitmap_height(ship_sprite);

        player_spawn(&players[i], 100,100);
    }

}

void player_spawn(Player *p, float x, float y){
     p->x = x;
     p->y = y;
     p->flags.flag.alive |= 1;
     p->flags.flag.collidible |= 1;
     p->flags.flag.invincible |= 0;
     p->flags.flag.visible |= 1;
     p->flags.flag.shoot |= 0;
     p->flags.flag.controller_disabled |= 1;

}

static void player_change_weapon(Player *p, int weapon){
    p->weapon = weapon < 0 ? 0 : weapon;
}

static void player_visible(Player *p, int vis){
    p->flags.flag.visible = p->flags.flag.visible ^ vis;
}

static WeaponConfig* player_select_weapon(int id){
    return &weapon_config[id];
}

static int player_keypressed(int pressed){
    return (int)keys[pressed];
}


void player_update_keyboard(ALLEGRO_EVENT *e){
    switch(e->type){
        case ALLEGRO_EVENT_KEY_DOWN:
            keys[e->keyboard.keycode] = TRUE;
        break;
        case ALLEGRO_EVENT_KEY_UP:
            keys[e->keyboard.keycode] = FALSE;
    }
}


static Player* player_selected(int num){
    return &players[num];
}

static void player_update_shot(float delta, Shot *shot){


    if(shot->alive && shot->x <= (window_get_width() + 50)){
        shot->x += shot->xv * delta;
        shot->y += shot->yv * delta;
    }

}

static void player_render_shot(Shot *shot){

    if(shot->alive){
        al_draw_bitmap_region(projectile_sprite.bmp.spritesheet,0,0,48,17,shot->x, shot->y,0);
    }
    //animation_draw(projectile_sprite,  shot->x, shot->y );
}

static void player_shoot(Shot list[MAX_SHOTS], float x, float y, float xv, float yv, Player *p){

    list[p->weapon].alive = TRUE;
    list[p->weapon].xv = xv;
    list[p->weapon].yv = yv;
    list[p->weapon].x = x;
    list[p->weapon].y = y;

}

static void player_update_mouse(ALLEGRO_EVENT *e, Player *p){


    p->x =  (mInput.x -  (p->w / 2));
    p->y =  (mInput.y - (p->h / 2));

    if(e->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP){
        if(e->mouse.button & 1){
            mInput.lButton = TRUE;
        }

        if(e->mouse.button & 2){
            mInput.rButton = TRUE;
        }

    }

}

static void player_do_shoot(Player *p, WeaponConfig *wcfg){
    player_shoot(shotList, p->x, p->y, 3.0f, 0, p  );
    sfx_play(wcfg[p->weapon].sfx);
}

static int player_mouse_button(ALLEGRO_EVENT *e, int button){
    return e->mouse.button & button;
}

void player_update(ALLEGRO_EVENT *e, float delta, int pnum){

    Player *selected = player_selected(pnum);
    WeaponConfig *weapon = player_select_weapon(selected->weapon);

    /*
    if(window_conf.use_mouse){
        player_update_mouse(e, selected);
    }





    if(player_mouse_button(e, PLAYER_MOUSE_LBUTTON) & !selected->shooting ){
        selected->shooting = TRUE;
        printf("Shoot!\n");
        player_do_shoot(selected, weapon);
    }
    */

    if(selected->flags.flag.controller_disabled & 1){

        if(player_keypressed(ALLEGRO_KEY_UP)){
            selected->y -= 0.5f * delta;
            old_dir = selected->direction;
            selected->direction = PLAYER_DIR_UP;
        }

        if(player_keypressed(ALLEGRO_KEY_DOWN)){
            selected->y += 0.5f * delta;
            old_dir = selected->direction;
            selected->direction = PLAYER_DIR_DN;
        }

        if(player_keypressed(ALLEGRO_KEY_LEFT)){
            selected->x -= 0.5f * delta;
            old_dir = selected->direction;
            selected->direction = PLAYER_DIR_LEFT;
        }

        if(player_keypressed(ALLEGRO_KEY_RIGHT)){
            selected->x += 0.5f * delta;
            old_dir = selected->direction;
            selected->direction = PLAYER_DIR_RIGHT;
        }

        if(player_keypressed(ALLEGRO_KEY_SPACE) && !selected->flags.flag.shoot & 1){
            selected->flags.flag.shoot |= 1;
            player_do_shoot(selected, weapon);
            weapon->extra_counter =  *game_timer_count + weapon->timer;
            printf("Shoot!\n");

        }

        if(player_keypressed(ALLEGRO_KEY_P) && !blink_on){
            blink_on = TRUE;
            blink_timer = *game_timer_count + 50;
            //player_change_weapon(selected, PLAYER_WLASER);
        }


        if(selected->flags.flag.shoot & 1){
            if(weapon->counter >= weapon->extra_counter){
                selected->flags.flag.shoot = selected->flags.flag.shoot ^ 1 ;
                printf("Ready for next shot!\n");
            }
        }

        /*
        if(selected->shooting){
            IncCounter(weapon->counter);
            if( weapon->counter >= weapon->timer){
                selected->shooting = FALSE;
                printf("Ready for next shot!\n");
                ResetCounter(weapon->counter);
            }
        }
        */


        if(blink_on){
             printf("BLINK ON ACTUAL_TIME: %d, END_TIME: %d \n", blink_count, blink_timer);
            if( blink_count >=  blink_timer ){
                blink_on = FALSE;
                printf("BLINK OFF\n");
                blink_count = 0;
            }
        }

        /*
        if(blink_on){
            IncCounter(blink_count);
            if(blink_count >= blink_timer){
               blink_on = FALSE;
               player_visible(selected,TRUE);
               ResetCounter(blink_count);
            }else {
                if( (blink_count % 40) == 0 ){
                    player_visible(selected,FALSE);
                }else {
                    player_visible(selected, TRUE);
                }

            }
        }
        */

        blink_count = (int) *game_timer_count;
        weapon->counter = (int) *game_timer_count;
        player_update_shot(delta, &shotList[0]);

    }

}


void player_render(int pnum){
    Player *p = player_selected(pnum);
    player_render_shot(&shotList[p->weapon]);

    if(p->flags.flag.visible & 1 && p->flags.flag.visible & 1){
        al_draw_scaled_bitmap(ship_sprite,0,0,200,90, p->x, p->y, 100,45,0);
    }
}

void player_disable_control_toggle(int pnum){
    Player *p = player_selected(pnum);
    p->flags.flag.controller_disabled ^= 1;
}
