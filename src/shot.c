#include "shot.h"


static void shot_init_single(Shot *b, int w, int h){
    b->alive = FALSE;
    b->delay = 0;
    b->x = 0;
    b->y = 0;
    b->xv = 0;
    b->yv = 0;
    b->w = w;
    b->h = h;
}

void shot_start(Shot bullet_list[MAX_SHOTS], animation_sprite *anim_sprite, const char *bulletname, const char *bullet_sprite){
    int i;

     animation_sprite_init_f(anim_sprite, 0.3,TRUE, bulletname, bullet_sprite);
     animation_add_frame(anim_sprite,0,0,0,0, 48,17);

    for(i = 0; i < MAX_SHOTS; i++){
        shot_init_single(&bullet_list[i], anim_sprite->bmp.width, anim_sprite->bmp.height);
    }
}
