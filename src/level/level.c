#include "level.h"
#include "../window/window.h"

extern ALLEGRO_DISPLAY *g_display;

static void* gm_image_loader(const char *path);
static unsigned int gid_flags(unsigned int gid);
static void gm_draw_single_layer(tmx_map *map, tmx_layer *layer);
static void gm_draw_layers(tmx_map *map, tmx_layer *layers);

void gm_init(game_map *map, const char *levelpath){
    map->level_map = tmx_load(levelpath);
    map->layer = map->level_map->ly_head;
    tmx_img_load_func = gm_image_loader;
    tmx_img_free_func = (void (*) (void*))al_destroy_bitmap;
    map->image = gm_render_to_bmp(map->level_map);
    map->x_delta = window_get_width() - al_get_bitmap_width(map->image);
    map->y_delta = window_get_height() - al_get_bitmap_height(map->image);

    //map->x_delta = window_get_width()

}


ALLEGRO_COLOR int_al_map_rgb(int col){
    unsigned int r,g,b;

    r = (col >> 16) & 0xFF;
    g = (col >> 8) & 0xFF;
    b = (col) & 0xFF;

    return al_map_rgb(r,g,b);
}

static void* gm_image_loader(const char *path){
    ALLEGRO_BITMAP *img = NULL;

    al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ANY);
    img  = al_load_bitmap(path);
    return (void*) img;
}

static unsigned int gid_flags(unsigned int gid){
    return gid & TMX_FLIP_BITS_REMOVAL;
}

static void gm_draw_single_layer(tmx_map *map, tmx_layer *layer){
        unsigned long i,j;
        unsigned int gid,x,y,w,h,flags;
        float opacity;

        tmx_tileset *tmx_ts;
        tmx_image *img;
        ALLEGRO_BITMAP *tileset;

        opacity = layer->opacity;

        for(i = 0; i < map->height;i++){
            for(j = 0; j < map->width; j++){
                gid = gid_flags( layer->content.gids[ (i*map->width) + j]);

                if(map->tiles[gid] != NULL){
                    tmx_ts = map->tiles[gid]->tileset;
                    img    = map->tiles[gid]->image;
                    x      = map->tiles[gid]->ul_x;
                    y      = map->tiles[gid]->ul_y;
                    w      = tmx_ts->tile_width;
                    h      = tmx_ts->tile_height;

                    if(img){
                        tileset = (ALLEGRO_BITMAP*) img->resource_image;
                    }else {
                        tileset = (ALLEGRO_BITMAP*) tmx_ts->image->resource_image;
                    }

                    flags = gid_flags( layer->content.gids[ (i*map->width) + j]);
                    al_draw_tinted_bitmap_region(tileset, al_map_rgba_f(opacity,opacity,opacity,opacity), x,y,w,h,j*tmx_ts->tile_width, i * tmx_ts->tile_height, flags);
                }
            }
        }


}

static void gm_draw_layers(tmx_map *map, tmx_layer *layers){
    while(layers){
        if(layers->visible){

            if(layers->type == L_GROUP){
                //todo
            }

            if(layers->type == L_OBJGR){
                //todo
            }

            if(layers->type == L_IMAGE){
                if(layers->opacity < 1.0f){
                    float op = layers->opacity;
                    al_draw_tinted_bitmap( (ALLEGRO_BITMAP*) layers->content.image->resource_image, al_map_rgba_f(op,op,op,op), 0,0,0);
                }
                al_draw_bitmap((ALLEGRO_BITMAP*) layers->content.image->resource_image, 0,0,0);
            }

            if(layers->type == L_LAYER){
                gm_draw_single_layer(map, layers);
            }


        }

        layers = layers->next;
    }
}


ALLEGRO_BITMAP* gm_render_to_bmp(tmx_map *map){
    ALLEGRO_BITMAP *bmp = NULL;
    unsigned int w,h;

    if(map->orient != O_ORT) return NULL;

    w = map->width * map->tile_width;
    h = map->height * map->tile_height;

    bmp = al_create_bitmap(w,h);

    if(!bmp) return NULL;

    al_set_target_bitmap(bmp);
    al_clear_to_color( int_al_map_rgb(map->backgroundcolor));
    gm_draw_layers(map, map->ly_head);

    al_set_target_bitmap(al_get_backbuffer(g_display));

    return bmp;


}



void gm_destroy(game_map *map){
    tmx_map_free(map->level_map);
    al_destroy_bitmap(map->image);

    map->level_map = 0x0;
    map->layer = 0x0;
    map->image = 0x0;

}
