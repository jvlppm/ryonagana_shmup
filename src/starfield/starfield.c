#include "starfield.h"
#include "../window/window.h"
#include "../utils/resources.h"
#include "../utils/utils.h"

extern ALLEGRO_DISPLAY *g_display;

#define MAX_STARS 100
#define STARS_VEL 0.9f

#define GRAY_STAR_COLOR al_map_rgb(42,42,42)

#define RAND_STARS(n)  ( (rand()  % n))

typedef enum STARS_TYPE {
	STAR_BIG = 0,
	STAR_WHITE,
	STAR_RED,
	STAR_BLUE,
	STAR_MINOR
}STARS_TYPE;


static void create_star(float x,float y, ALLEGRO_COLOR color);

typedef struct star {
    float x,y;
    int alive;
    int color;
}star;

star starfield[MAX_STARS];
star starfield_layer2[MAX_STARS];

static ALLEGRO_BITMAP *starfield_page;
static ALLEGRO_BITMAP *stars_sprite;

extern double current_time;

static float delay = 3.0f;
static int actual_delay = 100;
static int starfield_move = FALSE;



int starfield_moving = TRUE;
int starfield_counter = 0;
int starfield_timer = 50;




void starfield_init(void){
    int i;
    int color_variation = 0;
	
    starfield_page = NULL;
    stars_sprite   = NULL;

    starfield_page = al_create_bitmap(window_get_width(), window_get_height());
    stars_sprite = resource_load_sprite("assets/stars.bmp");

    ALLEGRO_ASSERT(stars_sprite);

    al_convert_mask_to_alpha(stars_sprite, al_map_rgb(255,0,255));
	//printf("%d", al_get_bitmap_height(stars_sprite));

    al_lock_bitmap(starfield_page, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);
    al_set_target_bitmap(starfield_page);

    for(i = 0; i < MAX_STARS;++i){
        float x = RAND_STARS(window_get_width() - 50);
        float y = RAND_STARS(window_get_height() - 50);

        create_star(x,y, GRAY_STAR_COLOR);
    }
    al_set_target_bitmap(al_get_backbuffer(g_display));
    al_unlock_bitmap(starfield_page);


    for(i = 0; i < MAX_STARS;++i){
        starfield[i].x = RAND_STARS(window_get_width());
        starfield[i].alive = 1;
        starfield[i].y =  RAND_STARS(window_get_height());

        color_variation = RAND_STARS(4);

        switch(color_variation){
            case 1:
                 starfield[i].color = STAR_BIG;
            case 0:
                 starfield[i].color = STAR_WHITE;
                 break;

            default:
            case 2:
                 starfield[i].color = STAR_RED;
                 break;
            case 3:
                 starfield[i].color = STAR_BLUE;
                 break;

            case 4:
                 starfield[i].color = STAR_MINOR;
                 break;


        }
    }
}

static void create_star(float x,float y, ALLEGRO_COLOR color){
    al_put_pixel(x,y,color);
    al_put_pixel(x+2,y+2,color);
    al_put_pixel(x+2,y,color);
    al_put_pixel(x,y+2,color);
}


void starfield_update(const float time, ALLEGRO_EVENT *ev){
    int i;



    if(starfield_moving){
        ++starfield_counter;
        if( starfield_counter >= starfield_timer ){
            starfield_counter = 0;
        }
    }



        if(starfield_moving){

            for(i = 0; i < MAX_STARS;i++){
                if(starfield_layer2[i].x > 0){
                      starfield_layer2[i].x -= 0.45;
                }

                if(starfield_layer2[i].x > 0 && starfield_layer2[i].alive ){
                     starfield_layer2[i].x = RAND_STARS(window_get_width() + 300);
                     starfield_layer2[i].y = RAND_STARS(window_get_height() + 300);

                }

                 starfield_layer2[i].alive = 0;
            }

            if(!starfield_layer2[i].alive){
                starfield_layer2[i].x = window_get_width();
                starfield_layer2[i].y = RAND_STARS(window_get_height());
                starfield_layer2[i].alive = 1;
            }
        }


        if(actual_delay > delay){
            for(i = 0; i < MAX_STARS;++i){
                    if(starfield[i].x > 0 ){
                       starfield[i].x -= STARS_VEL;

                    }

                    if(starfield[i].x <= 0 && starfield[i].alive ) {
                        starfield[i].x = RAND_STARS(window_get_width() + 300);
                        starfield[i].y = RAND_STARS(window_get_height() + 300);

                        int c = RAND_STARS(4);

                        switch(c){
                            case STAR_BIG:
                                 starfield[i].color = STAR_BIG;
                            case STAR_WHITE:
                                 starfield[i].color = STAR_WHITE;
                                 break;

                            default:
                            case STAR_RED:
                                 starfield[i].color = STAR_RED;
                                 break;
                            case STAR_BLUE:
                                 starfield[i].color = STAR_BLUE;
                                 break;

                            case STAR_MINOR:
                                 starfield[i].color = STAR_MINOR;
                                 break;


                        }


                        starfield[i].alive = 0;

                    }

                    if(!starfield[i].alive){
                        starfield[i].x = window_get_width();
                        starfield[i].y = RAND_STARS(window_get_height());
                        starfield[i].alive = 1;
                    }





            }



        }


}

void starfield_draw(void){
    int i;
	
    //back layer
    al_lock_bitmap(starfield_page, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READWRITE);
    al_set_target_bitmap(starfield_page);
    for(i = 0; i < MAX_STARS && starfield_layer2[i].alive ;i++){
        float x = RAND_STARS(window_get_width() - 50);
        float y = RAND_STARS(window_get_height() - 50);

        create_star(x,y, GRAY_STAR_COLOR);
    }
    al_set_target_bitmap(al_get_backbuffer(g_display));
    al_unlock_bitmap(starfield_page);


    // front layer
    for(i = 0; i < MAX_STARS;++i){
        switch(starfield[i].color){
        case STAR_BIG:
             al_draw_bitmap_region(stars_sprite,0,0,10,10, starfield[i].x,  starfield[i].y , 0);
            break;

        case STAR_WHITE:
             al_draw_bitmap_region(stars_sprite,0,0,10,10, starfield[i].x,  starfield[i].y , 0);
             break;

        default:
        case STAR_RED:
             create_star(starfield[i].x,starfield[i].y, al_map_rgb(255,255,255));
             //al_draw_bitmap_region(stars_sprite,0,17,10,6, starfield[i].x,  starfield[i].y , 0);
             break;
        case STAR_BLUE:
             create_star(starfield[i].x,starfield[i].y, al_map_rgb(66, 134, 244));
             break;

        case STAR_MINOR:
             al_draw_bitmap_region(stars_sprite,0,10,10,10, starfield[i].x,  starfield[i].y , 0);
             break;

        //al_put_pixel(starfield[i].x, starfield[i].y, starfield[i].color);
    }


    }



}

void starfield_destroy(void){

   if(starfield_page) al_destroy_bitmap(starfield_page);


}
