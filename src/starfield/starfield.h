#ifndef _STARFIELD_HEADER_
#define _STARFIELD_HEADER_

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>

void starfield_init(void);
void starfield_update(const float time, ALLEGRO_EVENT *ev);
void starfield_draw(void);
void starfield_destroy(void);

#endif // _STARFIELD_HEADER_
