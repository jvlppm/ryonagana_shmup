#include <stdio.h>
#include <time.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include "window/window.h"
#include "starfield/starfield.h"
#include "player.h"
#include "ui/ui.h"
#include "graphic/animation.h"
#include "graphic/fade.h"
#include "enemy.h"
#include "utils/utils.h"
#include "sound_res.h"

#include "game.h"



extern ALLEGRO_EVENT_QUEUE *g_queue;
extern ALLEGRO_DISPLAY *g_display;
extern ALLEGRO_KEYBOARD_STATE g_kbdstate;
extern ALLEGRO_BITMAP *g_screen;
extern ALLEGRO_TIMER *g_timer;
extern ALLEGRO_TIMER *timer_ingame;



extern Scene mainGame;


int redraw = 0;
int game_close = 0;
float step = 0;
double end_time = 0;
double start_time = 0;
float game_time = 0;
int gameover = FALSE;


/*
static void init_modules(){
    window_init();
    mixer_init();
    enemy_init();
    player_init();
    starfield_init();
    ui_init();
    scr_fade_init();
    sound_resource_init();


}
*/

/*
static void game_loop(ALLEGRO_EVENT *e, const float delta){
    //player_update(delta, e,PLAYER_ONE);
    ui_update();
    starfield_update(delta, e);
    enemy_update(delta, e);
    scr_fade_update(delta);

}
*/

int main(int argc, char *argv[]){
    srand(time(NULL));

    window_init();
    mixer_init();
    sound_resource_init();

    scene_load(&mainGame);

    /*
    init_modules();
    */



    int64_t timer_count = 0;

    start_time = al_get_time() * 1000;

    //window_config_save("config.dat");

    window_config_load("wincfg.dat");
    scene_init();


    while(!game_close){
        while( al_get_timer_count(g_timer) && (!gameover)){
            ALLEGRO_EVENT e;
            ALLEGRO_TIMEOUT timeout;
            al_init_timeout(&timeout, 0.006);
            al_wait_for_event_until(g_queue, &e, &timeout);

            step = end_time - start_time;
            start_time = end_time;

            if(e.type == ALLEGRO_EVENT_DISPLAY_CLOSE){
                gameover = 1;
                game_close = 1;
            }


            /*
            if(window_conf.use_mouse){
                if(e.type == ALLEGRO_EVENT_MOUSE_AXES || e.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY){
                    mInput.x = e.mouse.x;
                    mInput.y = e.mouse.y;
                }
            }
            */

            if(e.type == ALLEGRO_EVENT_TIMER){
                end_time = al_get_time() * 1000;
                timer_count = al_get_timer_count(g_timer);
                game_timer_count = &timer_count;


                 if(e.timer.source == g_timer){
                         scene_update(step, &e);
                         redraw = 1;
                 }
            }



            scene_update_keyboard(&e);
            /*
            player_update(&e, step, 0);
            ui_update();
            */


            if(redraw && al_is_event_queue_empty(g_queue)){
                redraw = 0;
                al_clear_to_color(al_map_rgb(0,0,0));


                scene_render();

                /*
                starfield_draw();
                player_render(0);
                ui_render();
                */

                al_flip_display();



            }


        }
    }


    window_config_save("wincfg.dat");
    scene_destroy();


    gracefully_quit();


}
