#ifndef _SOUND_RESOURCE_HEADER_
#define _SOUND_RESOURCE_HEADER_

#include <stdio.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include "utils/mixer.h"

#define MAX_GAME_SOUNDS 127


#define SFX_PLAYER_LASER1 0
#define SFX_PLAYER_BASIC  1
#define SFX_PLAYER_BASIC2 2
#define SFX_PLAYER_EXPLOSION1 3
#define SFX_PLAYER_EXPLOSION2 4


void sound_resource_init(void);
void sfx_play(int id);
void sound_resource_destroy();


#endif
