#ifndef _RESOURCES_HEADER_
#define _RESOURCES_HEADER

#include <stdio.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

ALLEGRO_BITMAP* resource_load_sprite(const char* str);
ALLEGRO_SAMPLE* resource_load_sound(const char *str);
ALLEGRO_FONT *resource_load_ttf(const char *path, int size, int flags);

#endif
