#ifndef DEBUG_HEADER
#define DEBUG_HEADER

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>


void debug_args_message(const char *fmt, ...);


#define XASSERT(x) do { \
					assert(x);\
					}while(0);\


#ifdef __GNUC__
#define DPRINT(x,...) debug_args_message(x, __VA_ARGS__)
#else
	#define DPRINT(x,...)
#endif

#endif // DEBUG_HEADER
