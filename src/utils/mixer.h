#ifndef _MIXER_HEADER_
#define _MIXER_HEADER_

#include <stdio.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>


typedef struct game_mixer{
    ALLEGRO_SAMPLE *sample;
    ALLEGRO_SAMPLE_INSTANCE *instance;
}game_mixer;

void mixer_init(void);
void mixer_destroy(void);

void game_mixer_load_sound(game_mixer *gm, const char *sound_path);
void game_mixer_play(game_mixer *gm);
void game_mixer_destroy(game_mixer *gm);


#endif
