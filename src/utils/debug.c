#include "debug.h"

void debug_args_message(const char *fmt, ...){

    do {
        va_list valist;
        char buffer[255];

        va_start(valist, fmt);
        vsprintf(buffer, fmt, valist);

        fprintf(stdout, "%s\n", buffer);

        va_end(valist);
    }while(0);

}

