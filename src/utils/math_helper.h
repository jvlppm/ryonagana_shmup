#ifndef MATH_HELPER_HEADER
#include <stdio.h>
#include <math.h>


#ifdef INLINE_FUNCTION
#undef INLINE_FUNCTION
#endif

#if defined(WIN32) || defined(_WIN32) || defined(_MINGW32_)
    #define INLINE_FUNCTION __inline
#elif defined(__GNUC__) || defined(__APPLE__)
    #define INLINE_FUNCTION inline
#endif


#ifndef M_PI
#define M_PI 3.14159265
#endif

#define DEG2RAD(DEG) ((DEG)*((M_PI)/(180.0)))
#define RAD2DEG(RAD) ((RAD)*180/M_PI)


double calculate_distance(float a, float b);
double rand_f(double min, double max);
#endif
