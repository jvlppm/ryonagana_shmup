#include "mixer.h"
#include "resources.h"

ALLEGRO_VOICE *voice;
ALLEGRO_MIXER *mixer;

void mixer_init(){
    al_reserve_samples(2);
    voice = al_create_voice(44100, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2);
    mixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2);

    al_attach_mixer_to_voice(mixer,voice);
}


void mixer_destroy(void){
    if(voice) al_destroy_voice(voice);
    if(mixer) al_destroy_mixer(mixer);
}



void game_mixer_load_sound(game_mixer *gm, const char *sound_path){
    gm->sample = al_load_sample(sound_path);
    if(!gm->sample){
        fprintf(stderr, "mixer.c: game_mixer_load_sound():  %s not found. sorry", sound_path);
        return;
    }
    gm->instance = al_create_sample_instance(gm->sample);
    al_attach_sample_instance_to_mixer(gm->instance,mixer);

}
void game_mixer_play(game_mixer *gm){
    if(!al_play_sample_instance(gm->instance)){
        fprintf(stdout, "Sound Error!");
        return;
    }
}
void game_mixer_destroy(game_mixer *gm){
    al_detach_sample_instance(gm->instance);
    al_destroy_sample(gm->sample);
    al_destroy_sample_instance(gm->instance);
}
