#include "utils.h"
#include <stdio.h>
#include "../window/window.h"
#include "mixer.h"

void gracefully_quit(void){

    mixer_destroy();
    window_close();

    fprintf(stdout, "\n==== GRACEFULLY QUITTED ===\n");
}
