#ifndef _ANIMATION_HEADER_
#define _ANIMATION_HEADER_

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro5.h>
#include "../utils/utils.h"

typedef union animation_all_flags {
    uint8_t  all_flags;

    struct
    {
        uint8_t loop_end : 1,
                repeat : 1,
                update_frame: 1;
    };



}animation_flags;

typedef struct animation_spritesheet {
    ALLEGRO_BITMAP *spritesheet;
    int width;
    int height;
    float x,y;
}animation_spritesheet;

typedef struct animation_frame {
    float x;
    float y;
    float offset_x;
    float offset_y;
    int w;
    int h;
    struct animation_frame *next;

}animation_frame;

typedef struct animation_sprite{
    char name[20];
    int repeat;
    int64_t counter;
    int64_t timer;
    int64_t end_counter;
    int frame_count;
    int current_frame;
    animation_flags flags;
    animation_spritesheet  bmp;
    animation_frame *frames, *head;

} animation_sprite;




void animation_play(animation_sprite *sprite);
void animation_stop(animation_sprite *sprite);

void animation_load_sprite(animation_sprite *spr, ALLEGRO_BITMAP *bmp,  float delay, int repeat, const char* anim_name);
void animation_sprite_init(animation_sprite *spr, const char *animation_name);
void animation_sprite_init_f(animation_sprite *spr, int64_t delay, int repeat, const char* anim_name, const char *spritesheet);
void animation_set(animation_sprite *spr, int64_t delay, int repeat);
void animation_sprite_destroy(animation_sprite *spr);
void animation_add_frame(animation_sprite *spr, float x, float y, float ox, float oy, int width, int height);
void animation_set_spritesheet(animation_sprite *spr, const char *image);
void animation_update(animation_sprite *spr, float time, ALLEGRO_EVENT *e);
void animation_draw(animation_sprite *spr, float x, float y);

#endif
