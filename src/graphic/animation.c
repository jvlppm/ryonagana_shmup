#include "animation.h"
#include "../utils/resources.h"
#include "../window/window.h"



static int animation_node_count(animation_sprite *spr){
    int count  = 1;
    animation_frame *frames = NULL;

    if(!spr->head){
        return 0;
    }

    frames = spr->head;

    do {
        if(!spr->frames || !frames->next)
            break;
        ++count;
        frames = frames->next;
    }while(frames->next != NULL);

    return count;
}




void animation_init(void){


}

void animation_load_sprite(animation_sprite *spr, ALLEGRO_BITMAP *bmp, float delay, int repeat, const char *anim_name){
    if(bmp == NULL){
        fprintf(stdout,"animation.c - animation_sprite_img() Image Ptr Empty");
        spr->bmp.spritesheet = al_create_bitmap(100,100);
        al_set_target_bitmap(spr->bmp.spritesheet);
        al_clear_to_color(al_map_rgb(255,255,255));
        al_set_target_bitmap(al_get_backbuffer(window_get_display()));
    }else {
        spr->bmp.spritesheet = al_clone_bitmap(bmp);
        al_convert_mask_to_alpha(spr->bmp.spritesheet, al_map_rgb(255,0,255));
        spr->bmp.width = al_get_bitmap_width(bmp);
        spr->bmp.height = al_get_bitmap_height(bmp);
        spr->bmp.x = 0;
        spr->bmp.y = 0;

    }

    animation_set(spr, delay, repeat);
    strcpy(spr->name, anim_name);





}

void animation_sprite_init(animation_sprite *spr, const char* animation_name){
   // animation_set(spr, 0,1, path, animation_name);
    animation_set(spr,9,1);
    strcpy(spr->name, animation_name);
}


void animation_sprite_init_f(animation_sprite *spr, int64_t delay, int repeat, const char* anim_name, const char *spritesheet){
    animation_set(spr,delay,repeat);
    animation_set_spritesheet(spr, spritesheet);
    strcpy(spr->name, anim_name);
}

void animation_set_spritesheet(animation_sprite *spr, const char *image){
    spr->bmp.spritesheet = resource_load_sprite(image);
    al_convert_mask_to_alpha(spr->bmp.spritesheet, al_map_rgb(255,0,255));

    if(!spr->bmp.spritesheet){
         spr->bmp.spritesheet = al_create_bitmap(100,100);
         al_set_target_bitmap(spr->bmp.spritesheet);
         al_clear_to_color(al_map_rgb(255,255,255));
         al_set_target_bitmap(al_get_backbuffer(window_get_display()));
    }

    spr->bmp.width = al_get_bitmap_width(spr->bmp.spritesheet);
    spr->bmp.height = al_get_bitmap_height(spr->bmp.spritesheet);
    spr->bmp.x = 0;
    spr->bmp.y = 0;
}


void animation_set(animation_sprite *spr, int64_t delay, int repeat){

    spr->counter =  0;
    spr->frames = 0x0;
    spr->repeat = repeat;
    spr->head = 0x0;
    spr->timer= delay;
    spr->flags.all_flags = 0U;
    spr->bmp.spritesheet = 0x0;
    spr->flags.repeat = (uint8_t) repeat;
    spr->end_counter = 5000;
    spr->flags.loop_end = 1;


 }

static void animation_frame_set(animation_frame **frame, float x, float y, float ox, float oy, int width, int height){

    animation_frame *tmp_frame;




    if(! *frame){
        tmp_frame = malloc(sizeof(animation_frame));
        tmp_frame->x = x;
        tmp_frame->y = y;
        tmp_frame->offset_x = ox;
        tmp_frame->offset_x = oy;
        tmp_frame->w = width;
        tmp_frame->h = height;
        tmp_frame->next = 0x0;
        *frame = tmp_frame;
    }

    tmp_frame = *frame;
    tmp_frame->next = 0x0;
    tmp_frame->x = x;
    tmp_frame->y = y;
    tmp_frame->offset_x = ox;
    tmp_frame->offset_x = oy;
    tmp_frame->w = width;
    tmp_frame->h = height;



}

void animation_add_frame(animation_sprite *spr, float x, float y, float ox, float oy, int width, int height){
    animation_frame *frames;

    if(!spr->frames){
        animation_frame_set(&spr->frames,x,y,ox,oy,width,height);
        spr->head = spr->frames;
        return;

    }

     frames =  NULL;
     animation_frame_set(&frames,x,y,ox,oy,width,height);
     spr->frames->next = frames;
     spr->frames = spr->frames->next;

}

void animation_update(animation_sprite *spr, float time, ALLEGRO_EVENT *e){


    if(!spr->flags.loop_end){
        ++(spr->counter);
        if(spr->counter >= spr->end_counter){
            if(spr->frames->next == NULL){

                if(!spr->flags.repeat){
                    return;
                }

                spr->frames = spr->head;

            }

            spr->frames = spr->frames->next;
            spr->counter = 0;
        }
    }

}

void animation_draw(animation_sprite *spr, float x, float y){
     al_draw_bitmap_region(spr->bmp.spritesheet, spr->frames->x, spr->frames->y, spr->frames->w, spr->frames->h, x ,y,0);

}


void animation_sprite_destroy(animation_sprite *spr){
    animation_frame *frames, *aux = NULL;

    frames = spr->head;

    do{

        aux = frames->next;
        free(frames);
        frames = NULL;
        frames = aux;


    }while(frames->next != NULL);

    frames = spr->head;

    if(frames){
        free(frames);
        frames = NULL;
        spr->frames = NULL;
    }


    if(spr->bmp.spritesheet){
        al_destroy_bitmap(spr->bmp.spritesheet);
    }
}


void animation_play(animation_sprite *sprite){
    sprite->end_counter = sprite->timer;
    sprite->flags.loop_end = FALSE;
    return;
}
void animation_stop(animation_sprite *sprite){
     sprite->flags.loop_end = TRUE;
    return;
}


