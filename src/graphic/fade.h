#ifndef _FADE_HEADER_
#define _FADE_HEADER_

#include <stdio.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>


typedef enum fade_type {
    FADE_IN = 0,
    FADE_OUT
}fade_type;


typedef struct fade_config {
    float alpha;
    int fade_finished;
    int64_t fade_counter;
    int fade_time;
    int mode;

}fade_config;



void scr_fade_init(int time);
void scr_fade_in(void);
void scr_fade_out(void);
void scr_fade_draw();
void scr_fade_update(void);
void scr_fade_destroy(void);


#endif
