#include "fade.h"
#include "../window/window.h"
#include "../utils/utils.h"
#include <math.h>

ALLEGRO_BITMAP *fade_screen = 0x0;
float percentage = 0;
float  alpha = 0;
int  fade_finished = TRUE;
uint64_t fade_time = 0;
uint64_t fade_counter = 0;

int mode = FADE_OUT;

void scr_fade_init(int time){
    fade_screen = al_create_bitmap(window_get_width(),window_get_height());
    fade_time = (uint64_t) time * 100;
}

void scr_fade_update(){


    if(!fade_finished){

        ++fade_counter;
         printf("%f fade alpha %ld counter\n", alpha, fade_counter);

        if(fade_counter >= fade_time){
            fade_finished = TRUE;
            fade_counter = 0;
            return;

        }

        switch(mode){
            case FADE_IN:
               alpha = (((fade_time - fade_counter) /  100.0f) / fade_time) * 100;
            break;

            case FADE_OUT:
                alpha = ((fade_time - fade_counter) / (double) (fade_time - fade_counter));
            break;

        }



    }
}

void scr_fade_in(void){
    mode = FADE_IN;
    fade_finished = FALSE;
    alpha = 1.0;
}


void scr_fade_out(void){
    mode = FADE_OUT;
    fade_finished = FALSE;
    alpha = 0.0;
 }


void scr_fade_draw(){
    switch(mode){

        default:
        case FADE_IN:
            al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
            al_draw_filled_rectangle(0,0, window_get_width(),  window_get_height(), al_map_rgba_f(0,0,0, alpha));
        break;

        case FADE_OUT:
            al_draw_filled_rectangle(0,0, window_get_width(),  window_get_height(), al_map_rgba_f(0,0,0, alpha));
        break;
    }
}



void scr_fade_destroy(void){

}
