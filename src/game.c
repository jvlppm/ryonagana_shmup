#include "game.h"
#include <allegro5/allegro5.h>

#include "player.h"
#include "starfield/starfield.h"
#include "ui/ui.h"
#include "graphic/animation.h"
#include "graphic/fade.h"
#include "enemy.h"

animation_sprite megaman;

static void G_init(){
    scr_fade_init(1);
    player_init();
    starfield_init();
    ui_init();
    enemy_init();


    animation_sprite_init_f(&megaman, 10, TRUE, "megaman", "assets//mega.bmp");
    animation_add_frame(&megaman, 0,0, 0,0,34,43);
    animation_add_frame(&megaman, 34,0,0,0,34,44);
    animation_add_frame(&megaman, 74,0,0,0,34,43);
}


static void G_update(const float delta, ALLEGRO_EVENT *e){
    starfield_update(delta, e);
    player_update(e,delta, 0);
    ui_update();
    scr_fade_update();
    enemy_update(delta, e);





}


static void G_render(){
    starfield_draw();
    player_render(0);
    animation_draw(&megaman, 100,400);
    enemy_draw();
    ui_render();
    scr_fade_draw();
}

static void G_destroy(){



    starfield_destroy();
    ui_destroy();
}

static void G_update_keyboard(ALLEGRO_EVENT *e){


    if(e->keyboard.keycode == ALLEGRO_KEY_Y){
        printf("Apertou Y\n");
        //animation_play(&megaman);
        scr_fade_in();



    }

    if(e->keyboard.keycode == ALLEGRO_KEY_U){
        printf("Apertou U\n");
        //animation_play(&megaman);
        player_disable_control_toggle(0);


    }

    player_update_keyboard(e);
    animation_update(&megaman,0.0f,e);

}

Scene mainGame = {
    &G_init,
    &G_update,
    &G_render,
    &G_update_keyboard,
    &G_destroy
};
