#ifndef _BULLET_HEADER_
#define _BULLET_HEADER_

#define MAX_SHOTS (10)

#include <stdio.h>
#include "window/window.h"
#include "graphic/animation.h"

typedef struct Shot {
    float x,y;
    float xv, yv;
    int alive;
    int w;
    int h;
    int delay;
    int type;
}Shot;



void shot_start(Shot bullet_list[MAX_SHOTS], animation_sprite *anim_sprite, const char *bulletname, const char *bullet_sprite);



#endif
