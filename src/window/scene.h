#ifndef SCENE_HEADER
#define SCENE_HEADER

#include <stdio.h>
#include <allegro5/allegro5.h>


typedef struct Scene {
    void (*init)();
    void (*update)(float delta, ALLEGRO_EVENT *e);
    void (*render)();
    void (*update_keyboard)(ALLEGRO_EVENT *e);
    void (*destroy)();

}Scene;


Scene actualScene;
Scene lastScene;


void scene_load(Scene *scene);
void scene_init();
void scene_render();
void scene_update(float delta, ALLEGRO_EVENT *e);
void scene_update_keyboard(ALLEGRO_EVENT *e);
void scene_destroy();
#endif
