#ifndef WINDOW_HEADER
#define WINDOW_HEADER

#include <stdio.h>
#include <allegro5/allegro5.h>
#include "../input/mouse.h"

void window_init();
void window_close();
void window_config_init(void);
void window_config_save(const char* filename);
void window_config_load(const char* filename);


int window_get_width();
int window_get_height();
char *window_get_title();

int64_t *game_timer_count;

ALLEGRO_DISPLAY* window_get_display();

ALLEGRO_TIMER *window_get_timer();
ALLEGRO_TIMER *window_get_timer_ingame();

typedef struct window_config {
    int width;
    int height;
    int fullscreen;
    int opengl;
    int use_mouse;
    char window_name[56];

}window_cfg;

window_cfg window_conf;



#define RESET_DISPLAY() (al_set_target_bitmap(al_get_backbuffer(window_get_display())))

MouseInput mInput;

#endif
