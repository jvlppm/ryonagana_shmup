#include "window.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include "../utils/utils.h"

#define TICKSPERFRAME 60.0f

ALLEGRO_EVENT_QUEUE *g_queue;
ALLEGRO_DISPLAY *g_display;
ALLEGRO_KEYBOARD_STATE g_kbdstate;
ALLEGRO_BITMAP *g_screen;
ALLEGRO_TIMER *g_timer;
ALLEGRO_TIMER *timer_ingame;

int isLoaded = 0;



window_cfg window_conf = {
    800,
    600,
    0,
    0,
    0,
    "SHMUP Project!"
};



int cyclecount = 0;


static int init_allegro(void) {


    if (!al_init()){

        //DEBUG_PRINT("Error: Cannot Initialize Allegro :-(");
        return -1;

    }

    if(!al_install_keyboard()){
        //DEBUG_PRINT(("Cannot Install Keyboard"));
        return -1;
    }

    if(!al_install_mouse()){
        return -1;
    }



    al_init_image_addon();
    al_init_font_addon();
    al_init_ttf_addon();
    al_init_primitives_addon();
    al_install_audio();
    al_install_mouse();
    al_init_acodec_addon();

    al_set_new_display_flags(ALLEGRO_WINDOWED);

    g_screen  =  al_create_bitmap(window_conf.width, window_conf.height);
    g_display =  al_create_display(window_conf.width, window_conf.height);




    al_get_keyboard_state(&g_kbdstate);
    al_set_target_bitmap(g_screen);
    al_set_target_bitmap(al_get_backbuffer(g_display));


    g_timer = al_create_timer(1.0f / TICKSPERFRAME  );


    timer_ingame = al_create_timer(0.1f);
    g_queue = al_create_event_queue();

    al_register_event_source(g_queue, al_get_keyboard_event_source());
    al_register_event_source(g_queue, al_get_mouse_event_source());
    al_register_event_source(g_queue, al_get_display_event_source(g_display));
    al_register_event_source(g_queue, al_get_timer_event_source(g_timer));

    /* register the ingame time   when the game starts  the timing starts */
    al_register_event_source(g_queue, al_get_timer_event_source(timer_ingame));

    al_set_window_title(g_display, window_conf.window_name);

    al_start_timer(g_timer);
    al_start_timer(timer_ingame);

    return 0;
}


void window_init()
{
	init_allegro();
	window_config_init();

    mInput.lButton = FALSE;
    mInput.rButton = FALSE;
    mInput.x = 0;
    mInput.y = 0;
    mInput.mWheelDown = 0;
    mInput.mWheelUp   = 0;

}
void window_close()
{

	if(g_queue) 		al_destroy_event_queue(g_queue);
	if(g_display) 		al_destroy_display(g_display);
	if(g_screen)  		al_destroy_bitmap(g_screen);
	if(g_timer)   		al_destroy_timer(g_timer);
	if(timer_ingame)  al_destroy_timer(timer_ingame);



}



void window_config_init(void)
{
    FILE *fp = fopen("wincfg.dat", "rb");

    if(!fp){
        window_config_save("wincfg.dat");
    }
    fclose(fp);
    window_config_load("wincfg.dat");
}

// after saves closes (use it when close )
void window_config_save(const char* filename)
{
	FILE *out = NULL;
	out = fopen(filename, "wb");
	fwrite(&window_conf, sizeof(unsigned char), sizeof(window_conf), out);
	fclose(out);

}
void window_config_load(const char* filename){
	FILE *in =  NULL;

	in = fopen(filename, "rb");
	
	memset(&window_conf, 0, sizeof(window_conf));
	fread(&window_conf, 1, sizeof(window_conf), in);



	fclose(in);
}


int window_get_width(){
    return window_conf.width;
}
int window_get_height(){
    return window_conf.height;
}

char *window_get_title(){
    char *title;
    size_t size = 0;

    size = strlen(window_conf.window_name);
    title = malloc( sizeof(char) *  (size + 1));
    strcpy(title, window_conf.window_name);
    return title;
}

ALLEGRO_DISPLAY* window_get_display(){
    return g_display;
}

ALLEGRO_TIMER *window_get_timer_ingame(){
    return timer_ingame;
}

ALLEGRO_TIMER *window_get_timer(){
    return g_timer;
}
