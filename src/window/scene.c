#include "scene.h"


void scene_init(){
    actualScene.init();
}

void scene_load(Scene *scene){
    actualScene = *scene;
}
void scene_render(){
    if(!actualScene.render) return;
    actualScene.render();
}
void scene_update(float delta, ALLEGRO_EVENT *e){
    if(actualScene.update == NULL) return;
    actualScene.update(delta, e);
}

void scene_update_keyboard(ALLEGRO_EVENT *e){
    if(actualScene.update_keyboard == NULL) return;
    actualScene.update_keyboard(e);
}

void scene_destroy(){
    if(actualScene.destroy == NULL) return;

    actualScene.destroy();
}
