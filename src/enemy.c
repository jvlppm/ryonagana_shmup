#include "enemy.h"
#include "window/window.h"
#include "utils/math_helper.h"
#include "player.h"
#include <allegro5/allegro_primitives.h>
extern ALLEGRO_DISPLAY *g_display;
//extern controlled_player player[2];


#define MAX_ENEMIES 100
#define MAX_ENEMY_SPRITE 16

enemy enemy_list[MAX_ENEMIES];



ALLEGRO_BITMAP* enemy_sprite[MAX_ENEMY_SPRITE];


static void enemy_bounds_enemyx(enemy *actualEnemy){
    if( (actualEnemy->x + actualEnemy->w)  <= 0){
        actualEnemy->x = window_get_width() + actualEnemy->w;
    }

    if( (actualEnemy->y + actualEnemy->h) < 0 ){
        actualEnemy->y = window_get_height() + actualEnemy->h;
    }

    if( (actualEnemy->y + actualEnemy->h) > window_get_height() ){
        actualEnemy->y = 0 - actualEnemy->h;
    }
}


void enemy_init(void)
{
	int i;
	
    memset(enemy_list,0x0, sizeof(enemy_list));
	
    //enemy_sprite[ENEMY_X] =  al_load_bitmap("assets//ship.bmp");
	
    if(!enemy_sprite[ENEMY_X]){
        enemy_sprite[ENEMY_X] = al_create_bitmap(120,40);

        al_set_target_bitmap(enemy_sprite[ENEMY_X]);
        al_clear_to_color(al_map_rgb(0,0,255));
        al_set_target_bitmap(al_get_backbuffer(g_display));

    }
	
    for (i = 0; i < MAX_ENEMIES; i++){
            enemy_list[i].x = window_get_width() / 2;
            enemy_list[i].y = window_get_height() / 2;
            enemy_list[i].alive = 1;
            enemy_list[i].vel_x = 1.0f;
            enemy_list[i].vel_y = 0.5f;
			enemy_list[i].enemy_type = ENEMY_X;
            enemy_list[i].angle = 1.0f;
            enemy_list[i].counter = 0;
            enemy_list[i].end_count = 5;
            enemy_list[i].flags.all_flags = 0U;
            enemy_list[i].flags.flag.alive |= 1;
            enemy_list[i].flags.flag.invincible |= 0;
            enemy_list[i].flags.flag.updated |= 1;
            enemy_list[i].flags.flag.visible |= 1;

            int type = enemy_list[i].enemy_type;

            enemy_list[i].w = al_get_bitmap_width(enemy_sprite[type]);
            enemy_list[i].h = al_get_bitmap_height(enemy_sprite[type]);
	}



}
void enemy_update(const float time, ALLEGRO_EVENT *ev)
{

    int i;
    for(i = 0; i < MAX_ENEMIES && enemy_list[i].alive;i++){
        switch(enemy_list[i].enemy_type){
            case ENEMY_X:
                enemy_brain_enemyx(time, &enemy_list[i]);
                break;

        }

    }
	
	
}

void enemy_draw(void)
{
    int i;
    float d;
    for(i = 0; i < MAX_ENEMIES && enemy_list[i].alive;i++){
        if(enemy_list[i].enemy_type == ENEMY_X){

         //al_draw_rotated_bitmap(enemy_sprite[ENEMY_X], enemy_list[i].x + 30 + enemy_list[i].h, enemy_list[i].y, 0,0, enemy_list[i].angle, 0 );
            al_draw_bitmap(enemy_sprite[ENEMY_X], enemy_list[i].x + 30 + enemy_list[i].h  , enemy_list[i].y, 0);
        }
    }


}


void enemy_brain_enemyx(const float time, enemy *actualEnemy ){

    actualEnemy->angle += 5 * time;

    actualEnemy->x += sin(actualEnemy->angle) * 5.0f;
    actualEnemy->y += cos(actualEnemy->angle) * 5.0f;


}
