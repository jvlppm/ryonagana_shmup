#ifndef PLAYER_HEADER
#define PLAYER_HEADER

#include <allegro5/allegro5.h>
#include "shot.h"

#define PLAYER_FLAG_NORMAL (0x2)
#define PLAYER_FLAG_INVINCIBLE (0x4)
#define PLAYER_FLAG_BLINKING (0x8)


#define IncCounter(x) ( ++(x))
#define ResetCounter(x) ((x) = 0)


#define PLAYER_WCANNON 0
#define PLAYER_WLASER  1

#define PLAYER_DIR_LEFT  0
#define PLAYER_DIR_RIGHT 1
#define PLAYER_DIR_UP    2
#define PLAYER_DIR_DN    3


typedef enum {
    PLAYER_MOUSE_LBUTTON = 1,
    PLAYER_MOUSE_RBUTTON = 2
}MOUSE_BUTTON;


typedef union player_flags_t {
    uint32_t all_flags;

    struct player_flags {
        uint32_t invincible : 1,
                blink : 1,
                collidible : 1,
                controller_disabled : 1,
                alive: 1,
                visible: 1,
                shoot : 1;
    };

    struct player_flags flag;
}player_flags_t;

typedef struct Player {
    float x,y;
    player_flags_t flags;
    unsigned char state;
    int direction;
    int bomb;
    int weapon;
    float life;
    int w;
    int h;
}Player;


typedef struct WeaponConfig {
    int id;
    int64_t extra_counter;
    int64_t counter;
    int64_t timer;
    float dmg;
    int sfx;

}WeaponConfig;




WeaponConfig weapon_config[MAX_SHOTS];


Player players[2];



void player_init();
void player_spawn(Player *p, float x, float y);
void player_update(ALLEGRO_EVENT *e, float delta, int pnum);
void player_update_keyboard(ALLEGRO_EVENT *e);
void player_render(int pnum);
void player_disable_control_toggle(int pnum);

#endif

