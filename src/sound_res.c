#include "sound_res.h"


game_mixer game_sfx[MAX_GAME_SOUNDS];

char *sfx_path[MAX_GAME_SOUNDS] = {
    "assets//sfx//laser1.wav",
    "assets//sfx//player_explosion.wav",
    "assets//sfx//explosion2.wav"
};




void sound_resource_init(void){
    int i;

    memset(game_sfx, 0, sizeof(game_mixer) * MAX_GAME_SOUNDS);

    for(i = 0; i < MAX_GAME_SOUNDS;i++){
        if(sfx_path[i] == NULL){
            break;
        }
        game_mixer_load_sound(&game_sfx[i], sfx_path[i]);
    }
}

void sfx_play(int id){
    game_mixer_play(&game_sfx[id]);
}

void sound_resource_destroy(){
    int i;
    for(i = 0; i < MAX_GAME_SOUNDS;i++){
        if(&game_sfx[i] != NULL){
            game_mixer_destroy(&game_sfx[i]);
        }
    }
}
