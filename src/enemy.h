#ifndef ENEMY_HEADER
#define ENEMY_HEADER

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include "player.h"


typedef enum enemy_type {
    ENEMY_X = 0
}enemy_type;

typedef union enemy_flags_t {
    uint32_t all_flags;

    struct e_flags {
        uint32_t alive : 1,
                 invincible : 1,
                 visible : 1,
                 updated : 1;
    };

    struct e_flags flag;


}enemy_flags_t;

typedef struct enemy {
    float x,y;
    float center_x, center_y;
    float vel_x, vel_y;
    float speed_x, speed_y;
    int w,h;
    int alive;
    float life;
    float angle;
    float rotation;
    enemy_flags_t flags;
    int64_t end_count;
    int64_t counter;
    int enemy_type;
}enemy;

void enemy_init(void);
void enemy_update(const float time, ALLEGRO_EVENT *ev);
void enemy_draw(void);


//brain
void enemy_brain_enemyx(const float time, enemy *actualEnemy);

#endif
