.PHONY: clean All

All:
	@echo "----------Building project:[ shmup - Debug ]----------"
	@"$(MAKE)" -f  "shmup.mk"
clean:
	@echo "----------Cleaning project:[ shmup - Debug ]----------"
	@"$(MAKE)" -f  "shmup.mk" clean
