##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=shmup
ConfigurationName      :=Debug
WorkspacePath          :=/home/ryonagana/projetos/c/shmup
ProjectPath            :=/home/ryonagana/projetos/c/shmup
IntermediateDirectory  :=bin/Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Nicholas
Date                   :=18/02/18
CodeLitePath           :=/home/ryonagana/.codelite
LinkerName             :=gcc
SharedObjectLinkerName :=gcc -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=bin/Debug/shmup
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="shmup.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)/usr/local/lib 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)asan $(LibrarySwitch)allegro $(LibrarySwitch)allegro_acodec $(LibrarySwitch)allegro $(LibrarySwitch)allegro_acodec $(LibrarySwitch)allegro_audio $(LibrarySwitch)allegro_color $(LibrarySwitch)allegro_font $(LibrarySwitch)allegro_image $(LibrarySwitch)allegro_main $(LibrarySwitch)allegro_memfile $(LibrarySwitch)allegro_primitives $(LibrarySwitch)allegro_ttf 
ArLibs                 :=  "asan" "allegro" "allegro_acodec" "allegro" "allegro_acodec" "allegro_audio" "allegro_color" "allegro_font" "allegro_image" "allegro_main" "allegro_memfile" "allegro_primitives" "allegro_ttf" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)/usr/local/include 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := gcc
CC       := gcc
CXXFLAGS :=  -Wextra -g -fsanitize=address -Wall  $(Preprocessors)
CFLAGS   :=  -Wall -g -fsanitize=address  -Wextra -pedantic $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/src_enemy.c$(ObjectSuffix) $(IntermediateDirectory)/src_player.c$(ObjectSuffix) $(IntermediateDirectory)/src_shmup.c$(ObjectSuffix) $(IntermediateDirectory)/src_window_window.c$(ObjectSuffix) $(IntermediateDirectory)/src_utils_debug.c$(ObjectSuffix) $(IntermediateDirectory)/src_starfield_starfield.c$(ObjectSuffix) $(IntermediateDirectory)/src_utils_utils.c$(ObjectSuffix) $(IntermediateDirectory)/src_ui_ui.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d bin/Debug || $(MakeDirCommand) bin/Debug


$(IntermediateDirectory)/.d:
	@test -d bin/Debug || $(MakeDirCommand) bin/Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_enemy.c$(ObjectSuffix): src/enemy.c $(IntermediateDirectory)/src_enemy.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/ryonagana/projetos/c/shmup/src/enemy.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_enemy.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_enemy.c$(DependSuffix): src/enemy.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_enemy.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_enemy.c$(DependSuffix) -MM src/enemy.c

$(IntermediateDirectory)/src_enemy.c$(PreprocessSuffix): src/enemy.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_enemy.c$(PreprocessSuffix) src/enemy.c

$(IntermediateDirectory)/src_player.c$(ObjectSuffix): src/player.c $(IntermediateDirectory)/src_player.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/ryonagana/projetos/c/shmup/src/player.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_player.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_player.c$(DependSuffix): src/player.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_player.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_player.c$(DependSuffix) -MM src/player.c

$(IntermediateDirectory)/src_player.c$(PreprocessSuffix): src/player.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_player.c$(PreprocessSuffix) src/player.c

$(IntermediateDirectory)/src_shmup.c$(ObjectSuffix): src/shmup.c $(IntermediateDirectory)/src_shmup.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/ryonagana/projetos/c/shmup/src/shmup.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_shmup.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_shmup.c$(DependSuffix): src/shmup.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_shmup.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_shmup.c$(DependSuffix) -MM src/shmup.c

$(IntermediateDirectory)/src_shmup.c$(PreprocessSuffix): src/shmup.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_shmup.c$(PreprocessSuffix) src/shmup.c

$(IntermediateDirectory)/src_window_window.c$(ObjectSuffix): src/window/window.c $(IntermediateDirectory)/src_window_window.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/ryonagana/projetos/c/shmup/src/window/window.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_window_window.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_window_window.c$(DependSuffix): src/window/window.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_window_window.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_window_window.c$(DependSuffix) -MM src/window/window.c

$(IntermediateDirectory)/src_window_window.c$(PreprocessSuffix): src/window/window.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_window_window.c$(PreprocessSuffix) src/window/window.c

$(IntermediateDirectory)/src_utils_debug.c$(ObjectSuffix): src/utils/debug.c $(IntermediateDirectory)/src_utils_debug.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/ryonagana/projetos/c/shmup/src/utils/debug.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_utils_debug.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_utils_debug.c$(DependSuffix): src/utils/debug.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_utils_debug.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_utils_debug.c$(DependSuffix) -MM src/utils/debug.c

$(IntermediateDirectory)/src_utils_debug.c$(PreprocessSuffix): src/utils/debug.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_utils_debug.c$(PreprocessSuffix) src/utils/debug.c

$(IntermediateDirectory)/src_starfield_starfield.c$(ObjectSuffix): src/starfield/starfield.c $(IntermediateDirectory)/src_starfield_starfield.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/ryonagana/projetos/c/shmup/src/starfield/starfield.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_starfield_starfield.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_starfield_starfield.c$(DependSuffix): src/starfield/starfield.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_starfield_starfield.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_starfield_starfield.c$(DependSuffix) -MM src/starfield/starfield.c

$(IntermediateDirectory)/src_starfield_starfield.c$(PreprocessSuffix): src/starfield/starfield.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_starfield_starfield.c$(PreprocessSuffix) src/starfield/starfield.c

$(IntermediateDirectory)/src_utils_utils.c$(ObjectSuffix): src/utils/utils.c $(IntermediateDirectory)/src_utils_utils.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/ryonagana/projetos/c/shmup/src/utils/utils.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_utils_utils.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_utils_utils.c$(DependSuffix): src/utils/utils.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_utils_utils.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_utils_utils.c$(DependSuffix) -MM src/utils/utils.c

$(IntermediateDirectory)/src_utils_utils.c$(PreprocessSuffix): src/utils/utils.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_utils_utils.c$(PreprocessSuffix) src/utils/utils.c

$(IntermediateDirectory)/src_ui_ui.c$(ObjectSuffix): src/ui/ui.c $(IntermediateDirectory)/src_ui_ui.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/ryonagana/projetos/c/shmup/src/ui/ui.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ui_ui.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ui_ui.c$(DependSuffix): src/ui/ui.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ui_ui.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ui_ui.c$(DependSuffix) -MM src/ui/ui.c

$(IntermediateDirectory)/src_ui_ui.c$(PreprocessSuffix): src/ui/ui.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ui_ui.c$(PreprocessSuffix) src/ui/ui.c


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r bin/Debug/


