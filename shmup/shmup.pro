TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DEFINES += _USE_MATH_DEFINES

SOURCES += \
    ../src/starfield/starfield.c \
    ../src/ui/ui.c \
    ../src/utils/debug.c \
    ../src/utils/utils.c \
    ../src/window/window.c \
    ../src/enemy.c \
    ../src/player.c \
    ../src/shmup.c \
    ../src/utils/math_helper.c \
    ../src/graphic/animation.c \
    ../src/input/keyboard.c \
    ../src/input/mouse.c \
    ../src/level/level.c \
    ../src/graphic/fade.c \
    ../src/utils/resources.c \
    ../src/utils/mixer.c \
    ../src/sound_res.c \
    ../src/utils/collision.c \
    ../src/shot.c \
    ../src/window/scene.c \
    ../src/game.c

HEADERS += \
    ../src/starfield/starfield.h \
    ../src/ui/ui.h \
    ../src/utils/debug.h \
    ../src/utils/utils.h \
    ../src/window/window.h \
    ../src/enemy.h \
    ../src/player.h \
    ../src/utils/math_helper.h \
    ../src/graphic/animation.h \
    ../src/input/keyboard.h \
    ../src/input/mouse.h \
    ../src/level/level.h \
    ../src/graphic/fade.h \
    ../src/utils/resources.h \
    ../src/utils/mixer.h \
    ../src/sound_res.h \
    ../src/utils/collision.h \
    ../src/shot.h \
    ../src/window/scene.h \
    ../src/window/window.h \
    ../src/game.h




unix:{
    INCLUDEPATH += /usr/local/include
    DEPENDPATH +=  /usr/local/include
    LIBS += -L/usr/local/lib -lallegro -lallegro_font -lallegro_ttf -lallegro_image -lallegro_primitives -lallegro_audio -lallegro_acodec -lm -lxml2 -lz

}

win32:{
 LIBS += -L$$PWD/../../../libs/allegro-5.0.10-mingw-4.7.0/lib/ -lallegro-5.0.10-monolith-mt-debug

}


INCLUDEPATH += $$PWD/../../../libs/allegro-5.0.10-mingw-4.7.0/include
DEPENDPATH += $$PWD/../../../libs/allegro-5.0.10-mingw-4.7.0/include

unix:!macx: LIBS += -L$$PWD/../src/libs/tmx/linux-build/ -ltmx

INCLUDEPATH += $$PWD/../src/libs/tmx/include
DEPENDPATH += $$PWD/../src/libs/tmx/include


win32: LIBS += -L$$PWD/../src/libs/tmx_win32/lib/ -ltmx

INCLUDEPATH += $$PWD/../src/libs/tmx_win32/include
DEPENDPATH += $$PWD/../src/libs/tmx_win32/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../src/libs/tmx_win32/lib/tmx.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../src/libs/tmx_win32/lib/libtmx.a
